# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 16:13:52 2016

@author: David Evans
"""

import numpy as np

def bellmanTmap(beta,w,pi,b,alpha,chi,Vcont,Qcont):
    '''
    Iterates of Bellman Equation in Problem 1
    See Homework 3.pdf for Documentation
    '''
   
    
    V_accept = w + beta *(alpha*Qcont + (1-alpha)*Vcont) 
    
    Q_effort = b - chi + beta * (.5* pi.dot(Vcont)+.5*Qcont)
    Q_noeffort = b + beta *(.3*pi.dot(Vcont)+.7*Qcont )
    Q = np.maximum(Q_effort,Q_noeffort)
    
    V = np.maximum(V_accept, Q)
    C =(V== V_accept) 

    E = (Q_effort == Q)
    
    return V,Q,C,E
    pass
    
    
    
def solveInfiniteHorizonProblem(beta,w,pi,b,alpha,chi,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem in Problem 1
    See Homework 3.pdf for Documentation
    '''
    V = np.zeros(len(w))
    Q = 0
    diff = 1.
    while diff > epsilon:
        V_new,Q_new,C,E =bellmanTmap(beta,w,pi,b,alpha,chi,V,Q)
        diff = np.linalg.norm(V-V_new)
        V=V_new
        Q = Q_new
    return V,Q,C,E
    pass
    
def HazardRate(pi,C,E):
    '''
    Computes the hazard rate of leaving unemployment in Problem 1
    See Homework 3.pdf for Documentation
    '''
    if E == 1:
        return pi.dot(C)*.5
    else :
        return (pi.dot(C)*.3)
    pass
    

        
        
def bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,Vcont,Qcont):
    '''
    Iterates of Bellman Equation of Problem 2
    See Homework 3.pdf for Documentation
    '''
    J = len(Qcont)
    S = len(Vcont[1,:])
   
    Q = np.zeros(J)
    V = np.zeros((J,S))
    Vwork = np.zeros((J,S))
    C = np.zeros((J,S))
       
   
    Q[0] = b + beta*(pi.dot(Vcont[0,:]))
    for j in range(1,J):
        Q[j] = b + beta*(p_h*(pi.dot(Vcont[j-1,:]))+(1-p_h)*(pi.dot(Vcont[j,:])))
       
    for j in range(J-1):
        Vfired = p_h * Qcont[j+1] + (1-p_h) * Qcont[j]
        Vnotfired   = p_h * Vcont[j+1,:] + (1-p_h) * Vcont[j,:]
        Vwork[j,:] = w*h[j] + beta * (alpha*Vfired + (1-alpha) * Vnotfired)
        V[j,:] = np.maximum(Vwork[j,:],np.ones(S)*Q[j])
       
    Vfired = p_h*Qcont[J-1] + (1-p_h)*Qcont[J-1]
    
    Vnotfired   = p_h*Vcont[J-1,:] + (1-p_h)*Vcont[J-1,:]
    
    Vwork[J-1,:] = w*h[J-1] + beta*(alpha*Vfired + (1-alpha)*Vnotfired)
    V[J-1,:] = np.maximum(Vwork[J-1,:],np.ones(S)*Q[J-1])
       
    for j in range(J):
        for s in range(S):
            if V[j,s] == Vwork[j,s]:
                C[j,s] = 1
              
    return V, Q, C

        
        
        
    
    
   
        
       
    pass

def solveInfiniteHorizonProblem_HC(beta,w,pi,b,alpha,p_h,h,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem of Problem 2
    See Homework 3.pdf for Documentation
    '''
    S = len(w)
    Lh = len(h)
    V =np.zeros((Lh,S))
    Q=np.zeros(Lh)
    diff = 1.
    while diff > epsilon:
        V_new,Q_new,C = bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,V,Q)
        diff = np.linalg.norm(V-V_new)
        V = V_new
        Q = Q_new
        
    return V,Q,C
    
    pass

    
    
    
    
    
    
    
    